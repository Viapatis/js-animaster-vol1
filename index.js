(function main() {
    document.getElementById('fadeInPlay')
        .addEventListener('click', function () {
            const block = document.getElementById('fadeInBlock');
            const { reset } = animaster().fadeIn(block, 5000);
            document.getElementById('fadeInReset').addEventListener('click', function () {
                reset();
            });
        });

    document.getElementById('movePlay')
        .addEventListener('click', function () {
            const block = document.getElementById('moveBlock');
            const { reset } = animaster().move(block, 1000, { x: 100, y: 10 });
            document.getElementById('moveReset').addEventListener('click', function () {
                reset();
            });
        });

    document.getElementById('scalePlay')
        .addEventListener('click', function () {
            const block = document.getElementById('scaleBlock');
            const { reset } = animaster().scale(block, 1000, 1.25);
            document.getElementById('scaleReset').addEventListener('click', function () {
                reset();
            });
        });
    document.getElementById('fadeOutPlay')
        .addEventListener('click', function () {
            const block = document.getElementById('fadeOutBlock');
            const { reset } = animaster().fadeOut(block, 1000, 1.25);
            document.getElementById('fadeOutReset').addEventListener('click', function () {
                reset();
            });
        });
    document.getElementById('moveAndHidePlay')
        .addEventListener('click', function () {
            const block = document.getElementById('moveAndHideBlock');
            const { reset } = animaster().moveAndHide(block, 1000, 1.25);
            document.getElementById('moveAndHideReset').addEventListener('click', function () {
                reset();
            });
        });
    document.getElementById('showAndHidePlay')
        .addEventListener('click', function () {
            const block = document.getElementById('showAndHideBlock');
            const { reset } = animaster().showAndHide(block, 1000, 1.25);
            document.getElementById('showAndHideReset').addEventListener('click', function () {
                reset();
            });
        });
    document.getElementById('heartBeatingPlay')
        .addEventListener('click', function () {
            const block = document.getElementById('heartBeatingBlock');
            const { stop, reset } = animaster().heartBeating(block, 1000, 1.25);
            document.getElementById('heartBeatingStop').addEventListener('click', function () {
                stop();
            });
            document.getElementById('heartBeatingReset').addEventListener('click', function () {
                reset();
            });
        });
    document.getElementById('shakingPlay')
        .addEventListener('click', function () {
            const block = document.getElementById('shakingBlock');
            const { stop, reset } = animaster().shaking(block, 1000, 1.25);
            document.getElementById('shakingStop').addEventListener('click', function () {
                stop();
            });
            document.getElementById('shakingReset').addEventListener('click', function () {
                reset();
            });
        });
    document.getElementById('castomPlay')
        .addEventListener('click', function () {
            const block = document.getElementById('castomBlock');
            const castomAnimation = animaster()
                .addMove(1000, { x: 100, y: 10 })
                .addMove(1000, { x: 50, y: 0 })
                .addScale(500, 1.25)
                .addFadeOut(500)
                .addMove(500, { x: 100, y: 10 })
                .addFadeIn(100);
            castomAnimation.play(block);
        });
    document.getElementById('castomHandlerBlock')
        .addEventListener('click',
            animaster()
                .addMove(1000, { x: 100, y: 10 })
                .addMove(1000, { x: 50, y: 0 })
                .addScale(500, 1.25)
                .addFadeOut(500)
                .addMove(500, { x: 100, y: 10 })
                .addFadeIn(500).buildHandler(false, true)
        );
})();

function animaster() {
    /**
     * Блок плавно появляется из прозрачного.
     * @param {HTMLElement} element         — HTMLElement, который надо анимировать
     * @param {number}      duration        — Продолжительность анимации в миллисекундах
     * @returns {{reset:function () {}}}    — Объект с функцией сброса в исходное положение
     */
    function fadeIn(element, duration) {
        element.style.transitionDuration = `${duration}ms`;
        element.classList.remove('hide');
        element.classList.add('show');
        return {
            reset: function () {
                resetFadeIn(element);
            }
        }
    }

    /**
     * Вовращает  блок в исходное положение, после  fadeIn
     * @param {HTMLElement} element     — HTMLElement, на котором нужно сбросить состояние
     */
    function resetFadeIn(element) {
        element.style.transitionDuration = null;
        element.classList.remove('show');
        element.classList.add('hide');
    }

    /**
     * Блок плавно становится прозрачным.
     * @param {HTMLElement} element         — HTMLElement, который надо анимировать
     * @param {number}      duration        — Продолжительность анимации в миллисекундах\
     * @returns {{reset:function () {}}}    — Объект с функцией сброса в исходное положение
     */
    function fadeOut(element, duration) {
        element.style.transitionDuration = `${duration}ms`;
        element.classList.remove('show');
        element.classList.add('hide');
        return {
            reset: function () {
                resetfadeOut(element);
            }
        }
    }

    /**
     * Вовращает  блок в исходное положение, после  fadeOut
     * @param {HTMLElement} element     — HTMLElement, на котором нужно сбросить состояние
     */
    function resetfadeOut(element) {
        element.style.transitionDuration = null;
        element.classList.remove('hide');
        element.classList.add('show');
    }

    /**
     * Функция, передвигающая элемент
     * @param {HTMLElement}         element         — HTMLElement, который надо анимировать
     * @param {number}              duration        — Продолжительность анимации в миллисекундах
     * @param {{x:number,y:number}} translation     — объект с полями x и y, обозначающими смещение блока
     * @returns {{reset:function () {}}}            — Объект с функцией сброса в исходное положение
     */
    function move(element, duration, translation) {
        element.style.transitionDuration = `${duration}ms`;
        if(element.style.transform.match(/translate\(\d+\.?\d*px, \d+\.?\d*px\)/)){
            element.style.transform=element.style.transform.replace(/translate\(\d+\.?\d*px, \d+\.?\d*px\)/,'');
        }
        element.style.transform +=getTransform(translation, null)+' ';
        return {
            reset: function () {
                resetMoveAndScale(element);
            }
        }
    }

    /**
     * Вовращает  блок в исходное положение, после  move
     * @param {HTMLElement} element     — HTMLElement, на котором нужно сбросить состояние
     */
    function resetMoveAndScale(element) {
        element.style.transitionDuration = null;
        element.style.transform = null;
    }

    /**
     * Функция, увеличивающая/уменьшающая элемент
     * @param {HTMLElement} element         — HTMLElement, который надо анимировать
     * @param {number}      duration        — Продолжительность анимации в миллисекундах
     * @param {number}      ratio           — во сколько раз увеличить/уменьшить. Чтобы уменьшить, нужно передать значение меньше 1
     * @returns {{reset:function () {}}}    — Объект с функцией сброса в исходное положение
     */
    function scale(element, duration, ratio) {
        element.style.transitionDuration = `${duration}ms`;
        if(element.style.transform.match(/scale\(\d+\.?\d*\)/)){
            element.style.transform=element.style.transform.replace(/scale\(\d+\.?\d*\)/,'');
        }
        element.style.transform +=getTransform(null, ratio)+' ';
        return {
            reset: function () {
                resetMoveAndScale(element);
            }
        }
    }

    /**
     * Функция, передвигающая элемент, который потом исчезает
     * @param {HTMLElement} element     — HTMLElement, который надо анимировать
     * @param {number}      duration    — Продолжительность анимации в миллисекундах
     */
    function moveAndHide(element, duration) {
        return this.addMove(duration * 0.4, { x: 100, y: 20 }).addFadeOut(duration * 0.6).play(element);
    }

    /**
     * Блок появляется,ждет и исчезает. Каждый шаг анимации длится 1/3 от времени, переданного аргументом в функцию.
     * @param {HTMLElement} element     — HTMLElement, который надо анимировать
     * @param {number}      duration    — Продолжительность анимации в миллисекундах
     */
    function showAndHide(element, duration) {
        return this
            .addFadeIn(duration / 3)
            .addDelay(duration / 3)
            .addFadeOut(duration / 3)
            .play(element);
    }

    /**
     * Имитация сердцебиения. Сначала элемент увеличится в 1.4 раза, потом обратно к 1. Каждый шаг анимации занимает 0.5 секунды.
     * @param {HTMLElement} element     — HTMLElement, который надо анимировать
     */
    function heartBeating(element) {
        return this.addScale(500, 1.4).addScale(500, 1).play(element, true);
    }

    /**
     * Дрожание элемента. Элемент двигается слева на право на 20px и возвращается обратно. Длительность анимации - 0.5 сек. 
     * @param {HTMLElement} element     — HTMLElement, который надо анимировать
     */
    function shaking(element) {
        this.move(element, 250, { x: 20, y: 0 });
        let handleTimeout = setTimeout(() => {
            this.move(element, 250, { x: 0, y: 0 });
        }, 250);
        const handleInterval = setInterval(() => {
            this.move(element, 250, { x: 20, y: 0 });
            handleTimeout = setTimeout(() => {
                this.move(element, 250, { x: 0, y: 0 });
            }, 250);
        }, 500);

        function stop() {
            clearInterval(handleInterval);
            clearInterval(handleTimeout);
        }

        return {
            stop: stop,
            reset: function () {
                stop();
                resetMoveAndScale(element);
            }
        };
    }


    /**
    * @param {number}              duration        — Продолжительность анимации в миллисекундах
    * @param {{x:number,y:number}} translation     — объект с полями x и y, обозначающими смещение блока
    */
    function addMove(duration, translation) {
        this._steps.push({
            name: 'move',
            duration: duration,
            translation: translation
        })
        return this;
    }
    /** 
     * делает ничего, в тичение заданого времени
     * @param {HTMLElement}         element         — HTMLElement, который надо анимировать
     * @param {number}              duration        — Продолжительность анимации в миллисекундах 
    */
    function empty(element, duration) {
        //тут ничего не происходит
        return { reset: empty }
    }

    /**
     * @param {number} duration 
     */
    function addDelay(duration) {
        this._steps.push({
            name: 'empty',
            duration: duration,
        })
        return this;
    }

    /**
     * @param {number} duration 
     * @param {number} ratio 
     */
    function addScale(duration, ratio) {
        this._steps.push({
            name: 'scale',
            duration: duration,
            ratio: ratio
        })
        return this;
    }

    /**
     * @param {number} duration 
     */
    function addFadeIn(duration) {
        this._steps.push({
            name: 'fadeIn',
            duration: duration,
        })
        return this;
    }

    /**
     * @param {number} duration 
     */
    function addFadeOut(duration) {
        this._steps.push({
            name: 'fadeOut',
            duration: duration,
        })
        return this;
    }

    /**
     * 
     * @param {HTMLElement} element 
     */
    function play(element, cycled, resetEnd) {
        let animationHandle;
        const animasterObject = this;
        function startAnimation(index, cycled, animasterObject) {
            const { duration, name, translation, ratio } = { ...animasterObject._steps[index] };
            animationHandle = setTimeout(() => {
                console.log(animasterObject);
                animasterObject._reset.unshift(animasterObject[name](element, duration, translation ? translation : ratio));
                if (index + 1 < animasterObject._steps.length) {
                    startAnimation(index + 1, cycled, animasterObject)
                } else if (cycled) {
                    startAnimation(0, cycled, animasterObject);
                }
            }, duration)
        }
        startAnimation(0, cycled, animasterObject);

        function reset(duration) {
            setTimeout(() => {
                stop();
                animasterObject._reset.forEach((resetAnimation) => {
                    resetAnimation.reset();
                })
                animasterObject._reset = [];
            }, duration ? duration : 0)
        }

        function stop() {
            clearInterval(animationHandle);
        }

        const returnObj = { reset: reset };

        if (resetEnd) {
            const sumDuration = this._steps.reduce((a, b) => a + b.duration, 500);
            reset(sumDuration);
        }
        if (cycled) {
            returnObj.stop = stop;
        }

        return returnObj;
    }
    /**
     * 
     * @param {boolean} cycled 
     * @param {boolean} resetEnd 
     */
    function buildHandler(cycled, resetEnd) {
        const animasterObject = this;
        return function () {
            animasterObject.play(this, cycled, resetEnd);
        }
    }

    function getTransform(translation, ratio) {
        const result = [];
        if (translation) {
            result.push(`translate(${translation.x}px,${translation.y}px)`);
        }
        if (ratio) {
            result.push(`scale(${ratio})`);
        }
        return result.join(' ');
    }
    return {
        scale: scale,
        move: move,
        fadeIn: fadeIn,
        fadeOut: fadeOut,
        moveAndHide: moveAndHide,
        showAndHide: showAndHide,
        heartBeating: heartBeating,
        shaking: shaking,
        addMove: addMove,
        addDelay: addDelay,
        addScale: addScale,
        addFadeIn: addFadeIn,
        addFadeOut: addFadeOut,
        play: play,
        empty: empty,
        buildHandler: buildHandler,
        _steps: [],
        _reset: []
    }
}
